FROM node:18

WORKDIR /app/frontend
COPY ./frontend/package*.json ./
RUN npm install
COPY ./frontend .
RUN npm run build

WORKDIR /app/bot
COPY ./backend/package*.json ./
RUN npm install
COPY ./backend .
RUN npm run build

EXPOSE 8088
EXPOSE 8044

CMD [ "npm","run" , "start:prod" ]