import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("settings")
export class Settings {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  sslCert: string

  @Column()
  sslKey: string

  @Column()
  sslEnabled: boolean

  @Column()
  synoToken: string

  @Column()
  openAIToken: string

  @Column({
    nullable: true,
  })
  synoUrl: string

}
