import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryColumn } from "typeorm";
import { User } from "./user.entity";

@Entity("channel")
export class Channel {

  @PrimaryColumn()
  id: number

  @ManyToOne(() => User, (user) => user.channels)
  creator: User

  @Column()
  name: string

  @Column()
  purpose: string

  @ManyToMany(() => User)
  @JoinTable()
  members: User[]
}