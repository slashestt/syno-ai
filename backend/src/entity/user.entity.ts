import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { Channel } from "./channel.entity";

@Entity("user")
export class User {

  @PrimaryColumn()
  id: number

  @Column()
  username: string

  @Column()
  dsm_uid: number

  @Column()
  disabled: boolean

  @Column()
  nickname: string

  @Column()
  email: string

  @OneToMany(() => Channel, (channel) => channel.creator)
  channels: Channel[]
}