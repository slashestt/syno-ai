import { DataSource } from "typeorm";
import { Settings } from "./entity/settings.entity";
import { Channel } from "./entity/channel.entity";
import { User } from "./entity/user.entity";

export const AppDataSource = new DataSource({
  type: "sqlite",
  database: "data.sqlite",
  synchronize: true,
  logging: true,
  entities: [ Settings, Channel, User ],
  subscribers: [],
  migrations: [],
});
