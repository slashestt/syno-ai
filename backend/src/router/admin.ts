import express, { Router } from 'express';
import { ADMIN_UI_PATH } from '../config';
import path from 'path';
import { getSettings, putSettings } from '../controller/admin';


const router : Router = Router()

router.use('/ui/', express.static(path.join(__dirname , ADMIN_UI_PATH)));

router.use(express.json());
router.get('/settings', getSettings);
router.put('/settings', putSettings);

export default router;