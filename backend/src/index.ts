import "reflect-metadata"
import http from "http";
import https from "https";
import api from "./app";
import { HTTPS_PORT, HTTP_PORT, getSettings } from "./config";
import { AppDataSource } from "./dataSource";

const app = api;

AppDataSource.initialize().then(async () => {
  const settings = await getSettings();
  // HTTP Server starten
  const httpServer = http.createServer(app);
  httpServer.listen(HTTP_PORT, () => {
    console.log("HTTP Server is running on port " + HTTP_PORT);
  });

  if (settings.sslEnabled) {
    const httpsOptions = {
      key: settings.sslKey,
      cert: settings.sslCert,
    };

    const httpsServer = https.createServer(httpsOptions, app);
    httpsServer.listen(HTTPS_PORT, () => {
      console.log("HTTPS Server is running on port " + HTTPS_PORT);
    });
  }
}).catch((err) => {
  console.error("Error during Data Source initialization", err);
});
