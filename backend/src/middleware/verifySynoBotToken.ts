import { NextFunction, Request, Response } from "express";
import { getSettings } from "../config";

async function verifySynoBotToken(req: Request, res: Response, next: NextFunction) {
  const token : string = req.body.token;
  const synoToken = (await getSettings()).synoToken;
  console.log(token);
  console.log(req.body);
  if (token === synoToken) {
    next();
  } else {
    res.sendStatus(401);
  }
}

export default verifySynoBotToken;