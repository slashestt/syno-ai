import { Request, Response } from "express";
import { AppDataSource } from "../dataSource";
import { Settings } from "../entity/settings.entity";

async function getSettings(req: Request, res: Response) {
  const settings = await AppDataSource.manager.findOneBy(Settings, {
    id: 1,
  });
  return res.json(settings);
}

async function putSettings(req: Request, res: Response) {
  const settings = await AppDataSource.getRepository(Settings).findOneBy({
    id: 1,
  });
  if (settings === null) {
    return;
  }
  AppDataSource.getRepository(Settings).merge(settings, req.body);
  const results = await AppDataSource.getRepository(Settings).save(settings);
  return res.json(results);
}

export { getSettings, putSettings };
