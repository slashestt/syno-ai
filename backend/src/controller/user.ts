import { Request, Response } from "express";
import { AppDataSource } from "../dataSource";
import { User } from "../entity/user.entity";

async function getUsers(req: Request, res: Response) {
  const users = await AppDataSource.manager.find(User);
  return res.json(users);
}