import { Request, Response } from "express";
import { SynoClient } from "../synoclient/synoclient";
import { OpenAIClient } from "../synoclient/openai";
import { ChatCompletionRequestMessage } from "openai";

const client = new SynoClient();
const aiClient = new OpenAIClient();

async function receiveChatMessage(req: Request, res: Response) {
  const correctChannelID = await client.getPrivateChannelID(Number(req.body.user_id));

  const posts = await client.listPosts({
    post_id: Number(req.body.post_id),
    next_count: 0,
    prev_count: 4,
    channel_id: correctChannelID
  });
  
  const context = aiClient.prepareContext(posts);
  const generatedMessage = await aiClient.generateText(context)  
  await client.sendMessage(Number(req.body.user_id),generatedMessage);

  res.sendStatus(200);
}

export {receiveChatMessage};


type IncomingChatMessage = {
  token: string,
  user_id: number,
  username: string,
  post_id: number,
  thread_id: number,
  timestamp: number,
  test: string,
}