import axios from "axios";
import { getSettings } from "../config";

export class SynoClient {
  
  public async sendMessage(userId: number, msg: string) {
    const payload = {
      text: msg,
      user_ids: [Number(userId)],
    };
    this.sendSynoMessage(payload, "chatbot");
  }

  public async listUsers() {
    const users_raw = await this.getSynoInfo("user_list");
    const users: SynoUser[] = users_raw.data.users;
    return users;
  }

  public async listPosts(p: SynoListPostPayload) {
    const url = (await getSettings()).synoUrl;
    const token = (await getSettings()).synoToken;
    const response = await axios.post(
      url,
      `method=post_list&token=${token}&channel_id=${p.channel_id}&next_count=${p.next_count}&prev_count=${p.prev_count}&post_id=${p.post_id}`,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );
    const posts : SynoPost[] = response.data.data.posts;
    return posts;
  }

  public async getChannelList() {
    const response = await this.getSynoInfo("channel_list");
    const channels: SynoChannel[] = response.data.channels;
    return channels;
  }

  private async getSynoInfo(method: string) {
    const url = (await getSettings()).synoUrl;
    const token = (await getSettings()).synoToken;
    const response = await axios.get(
      `${url}&method=${method}&token=${token}`
    );
    return response.data;
  }

  private async sendSynoMessage(payload: SynoPayload, method: string) {
    const url = (await getSettings()).synoUrl;
    const token = (await getSettings()).synoToken;
    const response = await axios.post(
      url,
      `method=${method}&token=${token}&payload=${JSON.stringify(payload)}`,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );
    console.log(response.data);
    return response.data;
  }

  public async getPrivateChannelID(user_id: number): Promise<number> {
    const channels = await this.getChannelList();
    for(const channel of channels) {
      if(channel.type !== "chatbot") {
        continue;
      }
      if(channel.total_member_count != 2){
        continue;
      }
      for(const member of channel.members) {
        if (member === user_id) {
          return channel.channel_id;
        }
      }
    }
    return -1;
  }
}

type SynoListPostPayload = {
  channel_id: number;
  next_count: number;
  prev_count: number;
  post_id: number;
};

type SynoPost = {
  channel_id: number;
  comment_count: number;
  create_at: number;
  creator_id: number;
  delete_at: number;
  message: string;
  post_id: number;
  thread_id: number;
  type: string;
  update_at: number;
};

type SynoPayload = {
  text: string;
  user_ids: number[];
  attachments?: SynoAttachment[];
};

type SynoAttachment = {
  callback_id: string;
  text: string;
  actions: SynoAction[];
};

type SynoAction = {
  type: "button";
  text: string;
  name: string;
  value: string;
  style: "green" | "grey" | "red" | "orange" | "blue" | "teal";
};

type SynoUser = {
  avatar_version: number;
  deleted: boolean;
  dsm_uid: number;
  first_time_login: boolean;
  human_type: string;
  is_disabled: boolean;
  nickname: string;
  status: "offline" | "online";
  type: string;
  user_id: number;
  user_props: {
    avatar_color: string;
    description: string;
    email: string;
    key_pair: {
      public_key: string;
    };
    timezone: string;
    timezoneUTC: string;
  };
  username: string;
};

type SynoChannel = {
  channel_id: number,
  create_at: number,
  creator_id: number,
  is_joined: boolean,
  last_view_comment_at: number,
  members: number[],
  name: string,
  purpose: string,
  total_member_count: number,
  type: string,
  unread: number,
  unread_comment: number,
  unread_thread: number,
}