import { ChatCompletionRequestMessage, Configuration, OpenAIApi } from "openai";
import { getSettings } from "../config";

export class OpenAIClient {
  async generateText(
    messages: ChatCompletionRequestMessage[]
  ): Promise<string> {
    try {
      const configuration = new Configuration({
        apiKey: (await getSettings()).openAIToken,
      });

      const openai = new OpenAIApi(configuration);

      const chatCompletion = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: messages,
      });
      const completed = chatCompletion.data.choices[0].message;
      console.log(chatCompletion.data);
      console.log(chatCompletion.data.choices);
      if (completed?.content) {
        return completed.content;
      }
    } catch (error) {
      console.error("Error:", error);
    }
    return "No response";
  }
  prepareContext(posts: {message: string}[]) {
    const context : ChatCompletionRequestMessage[] = [];
    for(const post of posts) {
      context.push({
        role: "user",
        content: post.message,
      })
    }
    return context;
  }
}
