import dotenv from 'dotenv';
import { AppDataSource } from './dataSource';
import { Settings } from './entity/settings.entity';

dotenv.config();
console.log(process.env);

const ADMIN_UI_PATH = '../../../frontend/dist'
const HTTP_PORT = 8088;
const HTTPS_PORT = 8044;

export {
  HTTPS_PORT,
  HTTP_PORT,
  ADMIN_UI_PATH
}

export async function getSettings(): Promise<Settings> {
  let settings = await AppDataSource.manager.findOneBy(Settings, {
    id: 1,
  });
  console.log(settings);
  if (settings !== null ) {
    return settings;
  }
  settings = AppDataSource.getRepository(Settings).create({
    id: 1,
    sslCert: '',
    sslKey:  '',
    sslEnabled: false,
    synoToken: 'demosynotoken',
    openAIToken: 'demoopenaitoken',
    synoUrl: "https://localhost:5001/webapi/entry.cgi?api=SYNO.Chat.External&version=2"
  })
  AppDataSource.getRepository(Settings).save(settings);
  return settings;
}
