import express from 'express';
import synologyRouter from './router/synology';
import adminRouter from './router/admin';

const app = express()

app.use('/synology/', synologyRouter);
app.use('/admin/', adminRouter);

export default app;