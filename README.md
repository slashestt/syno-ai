# Syno-AI
Custom Chatbot for Synology Chat to integrate OpenAI language models like ChatGPT.

## Features
- Web GUI for managing users, settings and statistics
- Enable the openai bot for all users or limit the access to specific users
- Specify the token limit of the context (to save on openai tokens -> save money)
- Specify the last message limit for the context (to save on openai tokens -> save money)
- Support for some public chat channels
- Simple docker deployment
- Deployable on a Synology NAS using docker without command line configurations
- Support for HTTPS without reverse proxy if required