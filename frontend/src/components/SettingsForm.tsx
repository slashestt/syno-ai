import { Button } from "@/components/ui/button";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Switch } from "@/components/ui/switch";

const formSchema = z.object({
  sslEnabled: z.boolean(),
  sslCert: z.string(),
  sslKey: z.string(),
});

function SettingsForm() {
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      sslCert: "",
      sslKey: "",
      sslEnabled: true,
    },
  });

  // 2. Define a submit handler.
  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    console.log(values);
  }
  return (
    <>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <FormField
            control={form.control}
            name="sslCert"
            render={({ field }) => (
              <FormItem>
                <FormLabel>SSL Certificate</FormLabel>
                <FormControl>
                  <Input {...field} />
                </FormControl>
                <FormDescription>
                  This is the content of the certificate file.
                </FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="sslKey"
            render={({ field }) => (
              <FormItem>
                <FormLabel>SSL Key</FormLabel>
                <FormControl>
                  <Input {...field} />
                </FormControl>
                <FormDescription>
                  This is the content of the key file.
                </FormDescription>
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="sslEnabled"
            render={({ field }) => (
              <FormItem>
                <FormLabel>SSL Key</FormLabel>
                <FormControl>
                  <Switch checked={field.value} onCheckedChange={field.onChange}></Switch>
                </FormControl>
                <FormDescription>
                  Decide, if ssl should be enabled.
                </FormDescription>
              </FormItem>
            )}
          />
          <Button type="submit">Submit</Button>
        </form>
      </Form>
    </>
  );
}

export default SettingsForm;
