import "./App.css";
import SettingsForm from "./components/SettingsForm";
import Layout from "./layout";

function App() {
  return (
    <>
      <Layout>
        <SettingsForm></SettingsForm>
      </Layout>
    </>
  );
}

export default App;
